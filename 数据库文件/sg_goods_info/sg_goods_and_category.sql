/*
 Navicat Premium Data Transfer

 Source Server         : Linux_docker_mysql5.7
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 192.168.243.128:3306
 Source Schema         : sg_goods

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 05/12/2023 15:54:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品的种类id',
  `c_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '类目的名称',
  `c_pid` int(11) NOT NULL COMMENT '类目的层级',
  `c_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '类目的背景图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '记录物品种类的表格数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES (1, '电子产品', 0, 'https://img0.baidu.com/it/u=1098514088,1178964595&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=588');
INSERT INTO `t_category` VALUES (2, '手机', 1, 'https://img2.baidu.com/it/u=2131765644,1059491842&fm=253&fmt=auto&app=138&f=JPEG?w=600&h=442');
INSERT INTO `t_category` VALUES (3, '手表', 1, 'https://img0.baidu.com/it/u=1557691983,2363698166&fm=253&fmt=auto&app=138&f=JPEG?w=750&h=500');
INSERT INTO `t_category` VALUES (4, '电脑', 1, 'https://img2.baidu.com/it/u=499026443,1159629320&fm=253&fmt=auto&app=138&f=JPEG?w=666&h=500');
INSERT INTO `t_category` VALUES (5, '平板', 1, 'https://p8.itc.cn/q_70/images01/20220225/16904fbf2e384342948c89079f54610c.jpeg');
INSERT INTO `t_category` VALUES (6, '学习用品', 0, 'https://img0.baidu.com/it/u=1411167466,1535837826&fm=253&fmt=auto&app=138&f=JPEG?w=530&h=5001');
INSERT INTO `t_category` VALUES (7, '书籍', 6, 'https://img1.baidu.com/it/u=3665989951,650132493&fm=253&fmt=auto&app=138&f=JPEG?https://img0.baidu.com/it/u=97011731,1918918474&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500');
INSERT INTO `t_category` VALUES (8, '乐器', 0, 'https://img0.baidu.com/it/u=2797499980,1167900478&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=583');
INSERT INTO `t_category` VALUES (9, '古风乐器', 8, 'https://img2.baidu.com/it/u=4289147371,284862295&fm=253&fmt=auto&app=138&f=JPEG?w=398&h=500');
INSERT INTO `t_category` VALUES (10, '西方乐器', 8, 'https://img2.baidu.com/it/u=3766470197,4282193756&fm=253&fmt=auto&app=138&f=JPEG?w=512&h=500');

-- ----------------------------
-- Table structure for t_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `g_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '商品名称',
  `g_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '商品描述',
  `g_price` decimal(12, 0) NOT NULL COMMENT '商品价格',
  `g_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '商品图片',
  `g_addTime` datetime(0) NOT NULL COMMENT '商品上架时间',
  `g_deleteTime` datetime(0) NULL DEFAULT NULL COMMENT '商品下架时间',
  `g_status` int(11) NOT NULL COMMENT '商品状态(1：卖出；0：未卖出)',
  `u_id` int(11) NOT NULL COMMENT '商品所对应的用户',
  `c_id` int(11) NOT NULL COMMENT '商品所对应的种类',
  `c_collect` int(11) NOT NULL DEFAULT 0 COMMENT '商品的收藏量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '商品信息的表数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_goods
-- ----------------------------
INSERT INTO `t_goods` VALUES (1, '小米手机', '九成新，着急出货', 1200, 'https://img1.baidu.com/it/u=2155416957,935404114&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=374', '2023-11-27 20:09:30', NULL, 0, 1, 2, 0);
INSERT INTO `t_goods` VALUES (2, '华为mate40手机', '八成新，有需要的联系', 1200, 'https://img100.51tietu.net/pic/2022080809/axdn3juxtb0.png', '2023-11-07 20:17:15', NULL, 0, 2, 2, 0);

SET FOREIGN_KEY_CHECKS = 1;
