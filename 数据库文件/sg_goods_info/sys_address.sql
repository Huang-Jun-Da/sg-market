/*
 Navicat Premium Data Transfer

 Source Server         : Linux_docker_mysql5.7
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 192.168.243.128:3306
 Source Schema         : sg_admin

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 05/12/2023 17:22:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_address
-- ----------------------------
DROP TABLE IF EXISTS `sys_address`;
CREATE TABLE `sys_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `a_province` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份',
  `a_city` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市',
  `a_school` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学校',
  `a_addTime` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '地址添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '记录地址信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_address
-- ----------------------------
INSERT INTO `sys_address` VALUES (1, '江西省', '南昌市', '南昌大学', '2023-08-07 16:23:20');
INSERT INTO `sys_address` VALUES (2, '江西省', '南昌市', '江西师范大学', '2023-10-09 16:25:45');
INSERT INTO `sys_address` VALUES (3, '湖南省', '长沙市', '中南大学', '2023-10-10 16:27:08');
INSERT INTO `sys_address` VALUES (4, '湖南省', '长沙市', '湖南大学', '2023-12-04 16:27:40');
INSERT INTO `sys_address` VALUES (5, '广东省', '广州市', '中山大学', '2023-11-27 16:28:34');
INSERT INTO `sys_address` VALUES (6, '广东省', '广州市', '华南理工大学', '2023-10-09 16:28:55');
INSERT INTO `sys_address` VALUES (7, '广西省', '南宁市', '广西大学', '2023-10-17 16:30:10');
INSERT INTO `sys_address` VALUES (8, '广西省', '南宁市', '广西医科大学', '2023-09-04 16:30:41');

SET FOREIGN_KEY_CHECKS = 1;
