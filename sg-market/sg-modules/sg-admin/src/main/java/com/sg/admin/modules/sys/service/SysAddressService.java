package com.sg.admin.modules.sys.service;


import com.sg.common.core.dto.SysAddressDTO;
import com.sg.common.core.entity.SysAddressEntity;
import com.sg.common.core.service.CrudService;


/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
public interface SysAddressService extends CrudService<SysAddressEntity, SysAddressDTO> {

}