package com.sg.admin.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sg.admin.modules.sys.dao.SysAddressDao;
import com.sg.common.core.dto.SysAddressDTO;
import com.sg.admin.modules.sys.service.SysAddressService;
import com.sg.common.core.entity.SysAddressEntity;
import com.sg.common.core.service.impl.CrudServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
@Service
public class SysAddressServiceImpl extends CrudServiceImpl<SysAddressDao, SysAddressEntity, SysAddressDTO> implements SysAddressService {

    @Override
    public QueryWrapper<SysAddressEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SysAddressEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}