package com.sg.admin.modules.sys.dao;


import com.sg.common.core.dao.BaseDao;
import com.sg.common.core.entity.SysAddressEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
@Mapper
public interface SysAddressDao extends BaseDao<SysAddressEntity> {
	
}