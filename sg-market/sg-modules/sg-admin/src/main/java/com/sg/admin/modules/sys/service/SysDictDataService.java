/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.sg.admin.modules.sys.service;

import com.sg.common.core.page.PageData;
import com.sg.common.core.service.BaseService;
import com.sg.admin.modules.sys.entity.SysDictDataEntity;
import com.sg.admin.modules.sys.dto.SysDictDataDTO;

import java.util.Map;

/**
 * 数据字典
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface SysDictDataService extends BaseService<SysDictDataEntity> {

    PageData<SysDictDataDTO> page(Map<String, Object> params);

    SysDictDataDTO get(Long id);

    void save(SysDictDataDTO dto);

    void update(SysDictDataDTO dto);

    void delete(Long[] ids);

}