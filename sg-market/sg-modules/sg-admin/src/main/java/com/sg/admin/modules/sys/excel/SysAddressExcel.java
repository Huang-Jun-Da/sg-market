package com.sg.admin.modules.sys.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
@Data
public class SysAddressExcel {
    @Excel(name = "地址id")
    private Integer id;
    @Excel(name = "省份")
    private String aProvince;
    @Excel(name = "城市")
    private String aCity;
    @Excel(name = "学校")
    private String aSchool;
    @Excel(name = "地址添加时间")
    private Date aAddtime;

}