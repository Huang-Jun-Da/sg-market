package com.sg.admin.modules.sys.controller;


import com.sg.admin.common.annotation.LogOperation;
import com.sg.common.core.dto.SysAddressDTO;
import com.sg.admin.modules.sys.service.SysAddressService;
import com.sg.common.core.constant.Constant;
import com.sg.common.core.page.PageData;
import com.sg.common.core.utils.Result;
import com.sg.common.core.validator.AssertUtils;
import com.sg.common.core.validator.ValidatorUtils;
import com.sg.common.core.validator.group.AddGroup;
import com.sg.common.core.validator.group.DefaultGroup;
import com.sg.common.core.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
@RestController
@RequestMapping("/sys/address")
@Api(tags="记录地址信息")
public class SysAddressController {
    @Autowired
    private SysAddressService sysAddressService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("sys:address:page")
    public Result<PageData<SysAddressDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<SysAddressDTO> page = sysAddressService.page(params);

        return new Result<PageData<SysAddressDTO>>().ok(page);
    }

    /**
     * 查询所有的地址
     * @return
     */
    @GetMapping("list")
//    @RequiresPermissions("sys:address:list")
    public Result<List<SysAddressDTO>> list(){
        List<SysAddressDTO> list = sysAddressService.list(new HashMap<>());

        return new Result<List<SysAddressDTO>>().ok(list);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    public Result<SysAddressDTO> get(@PathVariable("id") Long id){
        SysAddressDTO data = sysAddressService.get(id);

        return new Result<SysAddressDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("sys:address:save")
    public Result save(@RequestBody SysAddressDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        sysAddressService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("sys:address:update")
    public Result update(@RequestBody SysAddressDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        sysAddressService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("sys:address:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        sysAddressService.delete(ids);

        return new Result();
    }

//    @GetMapping("export")
//    @ApiOperation("导出")
//    @LogOperation("导出")
//    @RequiresPermissions("sys:address:export")
//    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
//        List<SysAddressDTO> list = sysAddressService.list(params);
//
//        ExcelUtils.exportExcelToTarget(response, null, list, SysAddressExcel.class);
//    }

}