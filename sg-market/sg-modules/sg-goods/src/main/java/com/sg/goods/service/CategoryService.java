package com.sg.goods.service;


import com.sg.common.core.service.CrudService;
import com.sg.goods.dto.CategoryDTO;
import com.sg.goods.entity.CategoryEntity;

import java.util.List;

/**
 * 记录物品种类的表格数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
public interface CategoryService extends CrudService<CategoryEntity, CategoryDTO> {

    List<CategoryDTO> categoryListByPid(Integer pId);
}