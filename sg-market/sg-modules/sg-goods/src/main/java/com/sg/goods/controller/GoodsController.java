package com.sg.goods.controller;


import com.sg.common.core.constant.Constant;
import com.sg.common.core.page.PageData;
import com.sg.common.core.utils.Result;
import com.sg.common.core.validator.AssertUtils;
import com.sg.common.core.validator.ValidatorUtils;
import com.sg.common.core.validator.group.AddGroup;
import com.sg.common.core.validator.group.DefaultGroup;
import com.sg.common.core.validator.group.UpdateGroup;
import com.sg.goods.dto.GoodsDTO;
import com.sg.goods.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 商品信息的表数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@RestController
@RequestMapping("/good")
@Api(tags="商品信息的表数据")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    public Result<PageData<GoodsDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<GoodsDTO> page = goodsService.page(params);

        return new Result<PageData<GoodsDTO>>().ok(page);
    }

    @GetMapping("list")
    @ApiOperation("查询所有的商品")
    public Result<List<GoodsDTO>> list(){
        List<GoodsDTO> list = goodsService.list(new HashMap<String,Object>());

        return new Result<List<GoodsDTO>>().ok(list);
    }


    @GetMapping("{id}")
    @ApiOperation("信息")
    public Result<GoodsDTO> get(@PathVariable("id") Long id){
        GoodsDTO data = goodsService.get(id);

        return new Result<GoodsDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    public Result save(@RequestBody GoodsDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        goodsService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    public Result update(@RequestBody GoodsDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        goodsService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        goodsService.delete(ids);

        return new Result();
    }


}