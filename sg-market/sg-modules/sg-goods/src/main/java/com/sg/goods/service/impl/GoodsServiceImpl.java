package com.sg.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sg.common.core.service.impl.CrudServiceImpl;
import com.sg.goods.dao.GoodsDao;
import com.sg.goods.dto.GoodsDTO;
import com.sg.goods.entity.GoodsEntity;
import com.sg.goods.service.GoodsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 商品信息的表数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Service
public class GoodsServiceImpl extends CrudServiceImpl<GoodsDao, GoodsEntity, GoodsDTO> implements GoodsService {

    @Override
    public QueryWrapper<GoodsEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<GoodsEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}