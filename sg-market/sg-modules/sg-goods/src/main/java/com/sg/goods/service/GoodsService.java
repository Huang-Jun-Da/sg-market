package com.sg.goods.service;


import com.sg.common.core.service.CrudService;
import com.sg.goods.dto.GoodsDTO;
import com.sg.goods.entity.GoodsEntity;

/**
 * 商品信息的表数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
public interface GoodsService extends CrudService<GoodsEntity, GoodsDTO> {

}