package com.sg.goods.dao;


import com.sg.common.core.dao.BaseDao;
import com.sg.goods.entity.GoodsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品信息的表数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Mapper
public interface GoodsDao extends BaseDao<GoodsEntity> {
	
}