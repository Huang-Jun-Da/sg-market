package com.sg.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

import java.math.BigDecimal;

/**
 * 商品信息的表数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Data
@ApiModel(value = "商品信息的表数据")
public class GoodsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "商品id")
	private Integer id;

	@ApiModelProperty(value = "商品名称")
	private String gName;

	@ApiModelProperty(value = "商品描述")
	private String gDescribe;

	@ApiModelProperty(value = "商品价格")
	private BigDecimal price;

	@ApiModelProperty(value = "商品图片")
	private String image;

	@ApiModelProperty(value = "商品上架时间")
	private Date addTime;

	@ApiModelProperty(value = "商品下架时间")
	private Date deleteTime;

	@ApiModelProperty(value = "商品状态(1：卖出；0：未卖出)")
	private Integer gStatus;

	@ApiModelProperty(value = "商品所对应的用户")
	private Integer uId;

	@ApiModelProperty(value = "商品所对应的种类")
	private Integer cId;

	@ApiModelProperty(value = "商品的收藏量")
	private Integer collect;


}