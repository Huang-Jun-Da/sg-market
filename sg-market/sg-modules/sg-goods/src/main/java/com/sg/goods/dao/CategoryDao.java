package com.sg.goods.dao;


import com.sg.common.core.dao.BaseDao;
import com.sg.goods.entity.CategoryEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 记录物品种类的表格数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Mapper
public interface CategoryDao extends BaseDao<CategoryEntity> {
	
}