package com.sg.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sg.common.core.service.impl.CrudServiceImpl;
import com.sg.common.core.utils.ConvertUtils;
import com.sg.goods.dao.CategoryDao;
import com.sg.goods.dto.CategoryDTO;
import com.sg.goods.entity.CategoryEntity;
import com.sg.goods.service.CategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import java.util.List;
import java.util.Map;

/**
 * 记录物品种类的表格数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Service
public class CategoryServiceImpl extends CrudServiceImpl<CategoryDao, CategoryEntity, CategoryDTO> implements CategoryService {
    @Autowired
    private CategoryDao categoryDao;

    @Override
    public QueryWrapper<CategoryEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CategoryEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    /**
     * 查询指定pId的商品种类
     * @param pId
     * @return
     */
    @Override
    public List<CategoryDTO> categoryListByPid(Integer pId) {
        QueryWrapper<CategoryEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pid",pId);
        List<CategoryEntity> entityList = categoryDao.selectList(queryWrapper);
        return ConvertUtils.sourceToTarget(entityList,CategoryDTO.class);
    }
}