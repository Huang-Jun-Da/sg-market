package com.sg.goods.controller;


import com.sg.common.core.constant.Constant;
import com.sg.common.core.page.PageData;
import com.sg.common.core.utils.Result;
import com.sg.common.core.validator.AssertUtils;
import com.sg.common.core.validator.ValidatorUtils;
import com.sg.common.core.validator.group.AddGroup;
import com.sg.common.core.validator.group.DefaultGroup;
import com.sg.common.core.validator.group.UpdateGroup;
import com.sg.goods.dto.CategoryDTO;
import com.sg.goods.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 记录物品种类的表格数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@RestController
@RequestMapping("/category")
@Api(tags="记录物品种类的表格数据")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 查询所有的类目
     * @return
     */
    @GetMapping("list")
    @ApiOperation("类目列表")
    public Result<List<CategoryDTO>> list(){
        List<CategoryDTO> list = categoryService.list(new HashMap<String, Object>());
        return new Result<List<CategoryDTO>>().ok(list);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    public Result<CategoryDTO> get(@PathVariable("id") Long id){
        CategoryDTO data = categoryService.get(id);

        return new Result<CategoryDTO>().ok(data);
    }

    /**
     * 查找指定pid的商品种类
     * @param pId
     * @return
     */
    @GetMapping("/search/{pId}")
    public Result<List<CategoryDTO>> getCategoryByPid(@PathVariable("pId") Integer pId){
        List<CategoryDTO> list = categoryService.categoryListByPid(pId);
        return new Result<List<CategoryDTO>>().ok(list);
    }



    @PostMapping
    @ApiOperation("保存")
    public Result save(@RequestBody CategoryDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        categoryService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    public Result update(@RequestBody CategoryDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        categoryService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        categoryService.delete(ids);

        return new Result();
    }
}