package com.sg.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 记录物品种类的表格数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Data
@ApiModel(value = "记录物品种类的表格数据")
public class CategoryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "商品的种类id")
	private Integer id;

	@ApiModelProperty(value = "类目的名称")
	private String name;

	@ApiModelProperty(value = "类目的层级")
	private Integer pid;

	@ApiModelProperty(value = "类目的背景图片")
	private String image;


}