package com.sg.goods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品信息的表数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Data
@TableName("t_goods")
public class GoodsEntity {

    /**
     * 商品id
     */
    @TableId(type = IdType.AUTO)
	private Integer id;
    /**
     * 商品名称
     */
	private String gName;
    /**
     * 商品描述
     */
	private String gDescribe;
    /**
     * 商品价格
     */
	private BigDecimal price;
    /**
     * 商品图片
     */
	private String image;
    /**
     * 商品上架时间
     */
	private Date addTime;
    /**
     * 商品下架时间
     */
	private Date deleteTime;
    /**
     * 商品状态(1：卖出；0：未卖出)
     */
	private Integer gStatus;
    /**
     * 商品所对应的用户
     */
	private Integer uId;
    /**
     * 商品所对应的种类
     */
	private Integer cId;
    /**
     * 商品的收藏量
     */
	private Integer collect;

    /**
     * 商品的地址_学校
     */
    private Integer aId;

}