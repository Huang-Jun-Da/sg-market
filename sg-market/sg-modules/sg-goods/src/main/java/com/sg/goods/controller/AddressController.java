package com.sg.goods.controller;

import com.sg.common.core.dto.SysAddressDTO;
import com.sg.common.core.utils.Result;
import com.sg.feign.api.service.RemoteAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前端首页获取所有的位置信息
 * @author tangjie
 * @create 2023-12-05 21:00
 */
@RestController
@RequestMapping("/address")
public class AddressController {
    @Autowired
    private RemoteAdminService remoteAdminService;

    /**
     * 查询所有的地址信息
     */
    @GetMapping("/list")
    public Result<List<SysAddressDTO>> list(){
       //调用远程调用接口
        return remoteAdminService.list();
    }
}
