package com.sg.goods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 记录物品种类的表格数据
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-04
 */
@Data
@TableName("t_category")
public class CategoryEntity {

    /**
     * 商品的种类id
     */
    @TableId(type = IdType.AUTO)
	private Integer id;
    /**
     * 类目的名称
     */
	private String name;
    /**
     * 类目的层级
     */
	private Integer pid;
    /**
     * 类目的背景图片
     */
	private String image;
}