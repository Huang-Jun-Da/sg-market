package com.sg.goods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.sg.feign.api.service")
public class SgGoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SgGoodsApplication.class, args);
    }

}
