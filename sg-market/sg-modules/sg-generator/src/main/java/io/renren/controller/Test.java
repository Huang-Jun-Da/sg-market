package io.renren.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName HuangJunDa
 * @Description TODO
 * @date 2023/12/3 0:51
 * @Version 1.0
 */

@RestController
@RequestMapping("/test")
public class Test {

    @RequestMapping("/test01")
    public String  test01(){
        return "test";
    }
}
