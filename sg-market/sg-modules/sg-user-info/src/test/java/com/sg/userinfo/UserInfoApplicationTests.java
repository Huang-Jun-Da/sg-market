package com.sg.userinfo;

import com.sg.userinfo.dto.RealNameDTO;
import com.sg.userinfo.entity.TUserEntity;
import com.sg.userinfo.service.RealNameService;
import com.sg.userinfo.service.TUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
class UserInfoApplicationTests {

    @Autowired
    private TUserService userService;

    @Autowired
    private RealNameService realNameService;
    @Test
    void contextLoads() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("page",1);
        map.put("limit",3);
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        System.out.println(page);
        System.out.println(limit);
    }

}
