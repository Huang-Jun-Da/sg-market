package com.sg.userinfo.controller;

import com.sg.common.core.constant.Constant;
import com.sg.common.core.page.PageData;
import com.sg.common.core.utils.Result;
import com.sg.common.core.validator.AssertUtils;
import com.sg.common.core.validator.ValidatorUtils;
import com.sg.common.core.validator.group.AddGroup;
import com.sg.common.core.validator.group.DefaultGroup;
import com.sg.common.core.validator.group.UpdateGroup;
import com.sg.userinfo.dto.RealNameDTO;
import com.sg.userinfo.entity.RealNameEntity;
import com.sg.userinfo.service.RealNameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-03
 */
@RestController
@RequestMapping("/realname")
@Api(tags = "")
public class RealNameController {
    @Autowired
    private RealNameService realNameService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    public Result<PageData<RealNameDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<RealNameDTO> page = realNameService.page(params);

        return new Result<PageData<RealNameDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    public Result<RealNameDTO> get(@PathVariable("id") Long id) {
        RealNameDTO data = realNameService.get(id);
        return new Result<RealNameDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    public Result save(@RequestBody RealNameDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        realNameService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    public Result update(@RequestBody RealNameDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        realNameService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        realNameService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<RealNameDTO> list = realNameService.list(params);
//        ExcelUtils.exportExcelToTarget(response, null, list, RealNameExcel.class);
    }


    /**
     *
     * @param realMap 前端传过来的验证信息，身份证，名字
     * @return 返回OK
     * @throws IOException Apache的远程调用出错，需要捕获
     */
    @PostMapping("/real")
    public Result realName(@RequestParam Map<String,String> realMap) throws IOException {
        // 获取前段传过来的身份证，名字
        String name = realMap.get("name");
        String idCard =realMap.get("idcard");
        // 校验名字长度是否为2-8位
        boolean isValidName = Pattern.matches("^[\\u4e00-\\u9fa5]{2,8}$", name);
        // 校验身份证长度是否为18位
        boolean isValidIdCard = Pattern.matches("^\\d{17}[\\dX]$", idCard);
        // 新建一个map传入到第三方验证，一定要idcard在前面，name在后面
        if (isValidName && isValidIdCard){
            HashMap<String, String> map = new HashMap<>();
            map.put("idcard",idCard);
            map.put("name",name);
            // 查看是否验证通过，这里会有异常传递，需要全局异常管理器
            boolean flag = realNameService.validName(map);
            if (flag){
                return new Result().ok("ok");
            }
        }
        return new Result().error("信息填写错误");
    }

    /**
     * 获取用户id即可查看用户是否实名认证
     * 返回用户名，身份证号码，是否实名
     * @return 用户DTO
     */
    @GetMapping("isRealName")
    @ApiOperation("信息")
    public Result<RealNameDTO> isRealName() {
        // todo 获取用户id
        String id = "1";
        // 查询用户认证信息
        RealNameDTO realName = realNameService.getRealName(id);
        // 返回用户认证信息
        return new Result<RealNameDTO>().ok(realName);
    }
}