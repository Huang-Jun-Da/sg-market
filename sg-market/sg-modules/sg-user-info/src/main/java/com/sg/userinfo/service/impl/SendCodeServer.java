package com.sg.userinfo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sg.common.core.utils.ValidPhone;
import com.sg.exception.exception.BusinessException;
import com.sg.userinfo.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 13637
 */

@Service
public class SendCodeServer {
    static String host = "https://gyytz.market.alicloudapi.com";
    static String path = "/sms/smsSend";
    static String method = "POST";
    static String appcode = "bcab0cfab97c44259dd62879152bb01f";
    public JSONObject sendCode(String phone, String randomCode) {

        boolean chinesePhone = ValidPhone.isChinesePhone(phone);
        if (!chinesePhone) {
            throw new BusinessException("号码有误");
        }

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", phone);
        querys.put("param", "**code**:" + randomCode + ",**minute**:5");
        querys.put("smsSignId", "2e65b1bb3d054466b82f0c9d125465e2");
        querys.put("templateId", "908e94ccf08b4476ba6c876d13f084ad");
        Map<String, String> bodys = new HashMap<String, String>();
        JSONObject jsonObject = null;
        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            String responseToString = EntityUtils.toString(response.getEntity());
            jsonObject = JSONObject.parseObject(responseToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
