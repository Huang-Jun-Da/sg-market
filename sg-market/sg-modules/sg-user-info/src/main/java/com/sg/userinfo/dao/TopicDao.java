package com.sg.userinfo.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sg.common.core.dao.BaseDao;
import com.sg.userinfo.dto.TopicDTO;
import com.sg.userinfo.entity.TopicEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-09
 */
@Mapper
public interface TopicDao extends BaseDao<TopicEntity> {

    IPage<TopicDTO> selectByPage(Page<TopicDTO> page);
}