package com.sg.userinfo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sg.common.core.page.PageData;
import com.sg.common.core.service.CrudService;
import com.sg.common.core.service.impl.CrudServiceImpl;
import com.sg.userinfo.dao.TopicDao;
import com.sg.userinfo.dto.TopicDTO;
import com.sg.userinfo.entity.TopicEntity;
import com.sg.userinfo.service.TopicService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-09
 */
@Service
public class TopicServiceImpl extends CrudServiceImpl<TopicDao, TopicEntity, TopicDTO> implements TopicService {

    @Autowired
    private TopicDao topicDao;

    @Override
    public QueryWrapper<TopicEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<TopicEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public IPage<TopicDTO> pageToDTO(Integer p,Integer l) {
        Page<TopicDTO> page = new Page<>(p,l);
        IPage<TopicDTO> list = topicDao.selectByPage(page);
        return list;
    }
}