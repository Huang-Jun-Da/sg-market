package com.sg.userinfo.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.org.apache.xpath.internal.operations.Bool;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-09
 */
@Data
@ApiModel(value = "")
public class TopicDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "是否关注")
    private Integer follow;

    @ApiModelProperty(value = "用户名")
    private String nickname;

    @JSONField(defaultValue = "true")
    @ApiModelProperty(value = "是否评论")
    private Boolean textBtn;

    @ApiModelProperty(value = "是否点赞")
    private Integer appreciateBtn;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "发布图片，多个用逗号隔开")
    private String image;

    @JSONField(format = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;

    @ApiModelProperty(value = "评论数量")
    private Integer comments;

    @ApiModelProperty(value = "点赞数量")
    private Integer thumbs;

    @ApiModelProperty(value = "经度")
    private BigDecimal lng;

    @ApiModelProperty(value = "纬度")
    private BigDecimal lat;



}