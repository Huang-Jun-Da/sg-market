package com.sg.userinfo.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;

@Configuration
public class FastjsonConfig {
    @Bean
    public HttpMessageConverters fastjsonHttpMessageConverters() {
        // 创建 Fastjson 消息转换器实例
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();

        // 创建 Fastjson 配置实例
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);

        // 设置 Fastjson 消息转换器配置
        fastConverter.setFastJsonConfig(fastJsonConfig);

        // 将 Fastjson 消息转换器加入 Spring Boot 内置的消息转换器列表中
        HttpMessageConverter<?> converter = fastConverter;
        return new HttpMessageConverters(converter);
    }
}