package com.sg.userinfo.controller;

import com.alibaba.fastjson.JSONObject;
import com.sg.userinfo.service.impl.SendCodeServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 13637
 */

@RestController
public class SendCodeController {

    @Autowired
    private SendCodeServer sendCodeServer;

    /**
     * 用于发送验证码，需要传入手机号（手机号已经做了校验），还需要自己传入验证码
     * @param phone 手机号
     * @param randomNum 验证码
     * @return {"msg":"成功","smsid":"170186432254421367743866184","code":"0","balance":"19"}
     */
    @GetMapping("/sendCode")
    public JSONObject senCodeById(String phone,String randomNum) {
        return sendCodeServer.sendCode(phone,randomNum);
    }
}
