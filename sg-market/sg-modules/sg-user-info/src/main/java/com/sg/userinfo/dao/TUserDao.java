package com.sg.userinfo.dao;


import com.sg.common.core.dao.BaseDao;
import com.sg.userinfo.entity.TUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */
@Mapper
public interface TUserDao extends BaseDao<TUserEntity> {

    Integer updateUser(TUserEntity userEntity);

    TUserEntity selectByUsername(String username);

    Integer updatePhoneById(@Param("id") Integer id,@Param("phone") String phone);
}