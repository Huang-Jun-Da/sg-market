package com.sg.userinfo.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sg.common.core.page.PageData;
import com.sg.common.core.service.CrudService;
import com.sg.userinfo.dto.TopicDTO;
import com.sg.userinfo.entity.TopicEntity;

import java.util.Map;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-09
 */
public interface TopicService extends CrudService<TopicEntity, TopicDTO> {

    IPage<TopicDTO> pageToDTO(Integer page, Integer limit);
}