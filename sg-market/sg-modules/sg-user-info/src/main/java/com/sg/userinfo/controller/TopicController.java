package com.sg.userinfo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sg.common.core.constant.Constant;
import com.sg.common.core.page.PageData;
import com.sg.common.core.utils.Result;
import com.sg.common.core.validator.AssertUtils;
import com.sg.common.core.validator.ValidatorUtils;
import com.sg.common.core.validator.group.AddGroup;
import com.sg.common.core.validator.group.DefaultGroup;
import com.sg.common.core.validator.group.UpdateGroup;
import com.sg.userinfo.dto.TopicDTO;
import com.sg.userinfo.service.TopicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-09
 */
@RestController
@RequestMapping("/topic")
@Api(tags = "")
public class TopicController {
    @Autowired
    private TopicService topicService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    public Result<IPage<TopicDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        Integer p = Integer.valueOf((String)params.get("page"));
        Integer l = Integer.valueOf((String)params.get("limit"));
        IPage<TopicDTO> page = topicService.pageToDTO(p, l);
        return new Result<IPage<TopicDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    public Result<TopicDTO> get(@PathVariable("id") Long id) {
        TopicDTO data = topicService.get(id);

        return new Result<TopicDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    public Result save(@RequestBody TopicDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        topicService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    public Result update(@RequestBody TopicDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        topicService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        topicService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<TopicDTO> list = topicService.list(params);
//        ExcelUtils.exportExcelToTarget(response, null, list, TopicExcel.class);
    }

}