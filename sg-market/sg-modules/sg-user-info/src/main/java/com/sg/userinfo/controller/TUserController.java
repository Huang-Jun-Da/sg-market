package com.sg.userinfo.controller;

import com.sg.common.core.constant.Constant;
import com.sg.common.core.page.PageData;
import com.sg.common.core.utils.Result;
import com.sg.common.core.validator.AssertUtils;
import com.sg.common.core.validator.ValidatorUtils;
import com.sg.common.core.validator.group.AddGroup;
import com.sg.common.core.validator.group.DefaultGroup;
import com.sg.common.core.validator.group.UpdateGroup;
import com.sg.exception.exception.BusinessException;
import com.sg.userinfo.dto.TUserDTO;
import com.sg.userinfo.entity.TUserEntity;
import com.sg.userinfo.service.TUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */

@Slf4j
@RestController
@RequestMapping("/user")
@Api(tags = "")
public class TUserController {
    @Autowired
    private TUserService userService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    public Result<PageData<TUserDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<TUserDTO> page = userService.page(params);
        return new Result<PageData<TUserDTO>>().ok(page);
    }

    /**
     * 从JWT获取用户ID，根据ID获取用户的详细信息展示
     *
     * @return 用户信息
     */
    @GetMapping("/singleUserInfo")
    @ApiOperation("信息")
    public Result<TUserDTO> get() {
        // todo 换成动态
        Long id = 1L;
        TUserDTO data = userService.getSingleUserInfo(id);
        return new Result<TUserDTO>().ok(data);
    }


    @PostMapping
    @ApiOperation("保存")
    public Result save(@RequestBody TUserDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        userService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    public Result update(@RequestBody TUserDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        userService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");
        userService.delete(ids);
        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<TUserDTO> list = userService.list(params);
//        ExcelUtils.exportExcelToTarget(response, null, list, TUserExcel.class);
    }


    /**
     * 修改信息包括：名字 | 性别 | 生日 | 个人简介
     *
     * @return 返回是否成功
     */
    @PutMapping("/modifySex")
    public Result modifyUserInfo(@RequestBody TUserDTO userDTO) {
        // todo 换成动态的
        userDTO.setId(1);
        boolean flag = userService.updateUserInfoById(userDTO);
        return new Result().ok(flag);
    }



    /**
     * 根据用户名查询出用户所有信息
     *
     * @param username 用户名
     * @return 用户实体类
     */
    @GetMapping("/usernameGetInfo/{username}")
    public TUserEntity usernameGetInfo(@PathVariable String username) {
        if (username == null) {
            throw new BusinessException("请输入用户名");
        }
        return userService.getUserInfoByUsername(username);
    }


    @GetMapping("/senCode")
    public Result senCode(@RequestParam Map<String, String> map) {
        // todo 换成动态的
        Integer id = 1;
        String phone = map.get("phone");
        boolean flag = userService.senCodeById(id, phone);
        return new Result<>().ok(flag);
    }

    @PutMapping("/replacePhone")
    public Result replacePhone(@RequestBody Map<String, String> map) {
        // todo 换成动态的
        Integer id = 1;
        String phone = map.get("phone");
        String code = map.get("code");
        boolean flag = userService.replacePhoneById(id, phone, code);
        return new Result().ok(flag);
    }
    @GetMapping("/test")
    public String test(@RequestParam String longitude){
        System.out.println(longitude);
        return System.currentTimeMillis()+"---";
    }
}