package com.sg.userinfo.dao;

import com.sg.common.core.dao.BaseDao;
import com.sg.userinfo.entity.RealNameEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-03
 */
@Mapper
public interface RealNameDao extends BaseDao<RealNameEntity> {
	RealNameEntity selectRealNameById(String id);

	String selectUserBirthday(Long id);
}