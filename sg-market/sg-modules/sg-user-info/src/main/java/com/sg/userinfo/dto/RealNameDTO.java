package com.sg.userinfo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-03
 */
@Data
@ApiModel(value = "")
public class RealNameDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer id;

	@ApiModelProperty(value = "用户id")
	private Integer userId;

	@ApiModelProperty(value = "用户名")
	private String name;

	@ApiModelProperty(value = "身份证号码")
	private String idcard;

	@ApiModelProperty(value = "是否成功(1是  0否 2未知)",name = "isRel")
	private Integer res;

	@ApiModelProperty(value = "性别(1男  0女)")
	private String sex;

	@ApiModelProperty(value = "生日")
	private String birthday;

	@ApiModelProperty(value = "地址")
	private String address;

	@ApiModelProperty(value = "是否实名")
	private Integer isRel;

}