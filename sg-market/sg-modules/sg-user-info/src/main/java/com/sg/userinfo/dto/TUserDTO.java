package com.sg.userinfo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */
@Data
@ApiModel(value = "")
public class TUserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private Integer id;

	@ApiModelProperty(value = "学校id")
	private String schoolId;

	@ApiModelProperty(value = "用户名")
	private String username;

	@ApiModelProperty(value = "密码")
	private String password;

	@ApiModelProperty(value = "用户昵称")
	private String nickname;

	@ApiModelProperty(value = "用户手机")
	private String phone;

	@ApiModelProperty(value = "用户生日")
	private String birthday;

	@ApiModelProperty(value = "用户性别")
	private String sex;

	@ApiModelProperty(value = "用户签名")
	private String userSign;

	@ApiModelProperty(value = "用户头像")
	private String avatar;

}