package com.sg.userinfo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-09
 */
@Data
@TableName("t_topic")
public class TopicEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer id;
    /**
     * 用户id
     */
	private Integer userId;
    /**
     * 内容
     */
	private String content;
    /**
     * 发布图片，多个用逗号隔开
     */
	private String image;
    /**
     * 发布时间
     */
	private Date publishTime;
    /**
     * 评论数量
     */
	private Integer comments;
    /**
     * 点赞数量
     */
	private Integer thumbs;
    /**
     * 经度
     */
	private BigDecimal lng;
    /**
     * 纬度
     */
	private BigDecimal lat;
}