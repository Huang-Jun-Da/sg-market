package com.sg.userinfo.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sg.common.core.service.impl.CrudServiceImpl;
import com.sg.common.core.utils.ConvertUtils;
import com.sg.exception.exception.BusinessException;
import com.sg.userinfo.dao.RealNameDao;
import com.sg.userinfo.dto.RealNameDTO;
import com.sg.userinfo.entity.RealNameEntity;
import com.sg.userinfo.service.RealNameService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-03
 */
@Service
@Slf4j
public class RealNameServiceImpl extends CrudServiceImpl<RealNameDao, RealNameEntity, RealNameDTO> implements RealNameService {

    @Autowired
    private RealNameDao realNameDao;

    @Override
    public QueryWrapper<RealNameEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");
        QueryWrapper<RealNameEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);
        return wrapper;
    }


    /**
     * 通过调用第三方接口实现实名认证
     *
     * @param realMap 认证信息
     * @return 是否成功
     * @throws IOException Apache的远程调用错误
     */
    @Override
    public boolean validName(Map<String, String> realMap) throws IOException {
        // 远程调用地址,第三方接口的密钥
        String url = "https://eid.shumaidata.com/eid/check";
        String appCode = "bcab0cfab97c44259dd62879152bb01f";
        // 进行认证
        String realNameText = postForm(appCode, url, realMap);

        // 将获取到的返回数据转成JSON对象
        JSONObject realNameJsonText = JSON.parseObject(realNameText);
        // 获取返回码是否为0，为0说明调用成功，不为0抛异常
        String code = realNameJsonText.getString("code");
        if (!"0".equals(code)) {
            // 如果不是0，记录返回失败的信息到日志中
            log.error(code);
            throw new BusinessException("网络连接错误");
        }
        // 获取返回的认证信息
        JSONObject result = realNameJsonText.getJSONObject("result");
        // 将result的信息转成DTO对象
        RealNameDTO realNameDTO = JSON.toJavaObject(result, RealNameDTO.class);
        // 记录一下信息
//        log.debug(String.valueOf(realNameDTO));
        // 如果不是1说明有问题，身份信息不通过
        if (!realNameDTO.getRes().equals(1)) {
            throw new BusinessException("身份验证出错，请检查");
        }
        // 将身份证号码加密
//        String idCarMd5 = StringToMD5Utils.toMD5(realNameDTO.getIdcard());
        // 将DTO转成Entity对象
        RealNameEntity realNameEntity = ConvertUtils.sourceToTarget(realNameDTO, RealNameEntity.class);
        // 设置用户ID
        realNameEntity.setUserId(1);
        // 设置创建时间
        realNameEntity.setCreateTime(new Date());
        // 实名认证成功
        realNameEntity.setIsRel(realNameDTO.getRes());

        // 将生日信息修改好
        String birthday = realNameDTO.getBirthday();
        String birthdayEntity = birthday.substring(0, 4) + "-" + birthday.substring(4, 6) + "-" + birthday.substring(6, 8);
        realNameEntity.setBirthday(birthdayEntity);
        // 将实名认证信息存入数据库
        baseDao.insert(realNameEntity);
        return true;
    }

    /**
     * 返回的信息
     * {
     * "code": "0", //返回码，0：成功，非0：失败（详见错误码定义）
     * //当code=0时，再判断下面result中的res；当code!=0时，表示调用已失败，无需再继续
     * "message": "成功", //返回码说明
     * "result": {
     * "name": "冯天", //姓名
     * "idcard": "350301198011129422", //身份证号
     * "res": "1", //核验结果状态码，1 一致；2 不一致；3 无记录
     * "description": "一致",  //核验结果状态描述
     * "sex": "男",
     * "birthday": "19940320",
     * "address": "江西省南昌市东湖区"
     * }
     * }
     */
    private String postForm(String appCode, String url, Map<String, String> params) throws IOException {
        OkHttpClient client = new OkHttpClient.Builder().build();
        FormBody.Builder formbuilder = new FormBody.Builder();
        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            formbuilder.add(key, params.get(key));
        }
        FormBody body = formbuilder.build();
        Request request = new Request.Builder().url(url).addHeader("Authorization", "APPCODE " + appCode).post(body).build();
        Response response = client.newCall(request).execute();
        System.out.println("返回状态码" + response.code() + ",message:" + response.message());
        return response.body().string();
    }

    /**
     * 查询用户是否实名认证
     * @param id 用户id
     * @return 实名认证信息
     */
    @Override
    public RealNameDTO getRealName(String id) {
        // 调用dao查询信息
        RealNameEntity realName = realNameDao.selectRealNameById(id);
        // 如果==1已经实名，封装用户信息进去
        if (realName.getIsRel() == 1){
            String idcard = realName.getIdcard();
            // 加密用户身份证号码
            String encryption = idcard.substring(0, 3) + "******" + idcard.substring(15);
            realName.setIdcard(encryption);
        }
        return ConvertUtils.sourceToTarget(realName, RealNameDTO.class);
    }
}