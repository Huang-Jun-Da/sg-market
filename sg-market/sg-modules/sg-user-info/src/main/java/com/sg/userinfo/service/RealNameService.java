package com.sg.userinfo.service;


import com.sg.common.core.service.CrudService;
import com.sg.userinfo.dto.RealNameDTO;
import com.sg.userinfo.entity.RealNameEntity;

import java.io.IOException;
import java.util.Map;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-03
 */
public interface RealNameService extends CrudService<RealNameEntity, RealNameDTO> {

    boolean validName(Map<String,String> realMap) throws IOException;

    RealNameDTO getRealName(String id);
}