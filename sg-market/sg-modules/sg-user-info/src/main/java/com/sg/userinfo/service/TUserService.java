package com.sg.userinfo.service;

import com.sg.common.core.service.CrudService;
import com.sg.userinfo.dto.TUserDTO;
import com.sg.userinfo.entity.TUserEntity;

/**
 * 
 *
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */
public interface TUserService extends CrudService<TUserEntity, TUserDTO> {

    TUserDTO getSingleUserInfo(Long id);

    boolean updateUserInfoById(TUserDTO userDTO);

    TUserEntity getUserInfoByUsername(String username);

    boolean senCodeById(Integer id, String phone);

    boolean replacePhoneById(Integer id, String phone, String code);
}