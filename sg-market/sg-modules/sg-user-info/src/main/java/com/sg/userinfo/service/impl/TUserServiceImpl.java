package com.sg.userinfo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.sg.common.core.service.impl.CrudServiceImpl;
import com.sg.common.core.utils.ConvertUtils;
import com.sg.common.core.utils.ValidPhone;
import com.sg.exception.exception.BusinessException;
import com.sg.userinfo.dao.TUserDao;
import com.sg.userinfo.dto.TUserDTO;
import com.sg.userinfo.entity.TUserEntity;
import com.sg.userinfo.service.TUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Map;

/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */
@Service
@Slf4j
public class TUserServiceImpl extends CrudServiceImpl<TUserDao, TUserEntity, TUserDTO> implements TUserService {

    public final String REDIS_CODE_KEY = "sg:redis:replacePhone:code:%s:%s";

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private TUserDao userDao;

    @Autowired
    private SendCodeServer sendCodeServer;

    @Override
    public QueryWrapper<TUserEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<TUserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    /**
     * 查询用户信息并加密返回
     *
     * @param id 用户ID
     * @return 用户信息
     */
    @Override
    public TUserDTO getSingleUserInfo(Long id) {
        // 根据用户ID获取用户的所有信息
        TUserEntity entity = baseDao.selectById(id);
        // 将Entity转成DTO
        TUserDTO userDTO = ConvertUtils.sourceToTarget(entity, currentDtoClass());
        // 获取用户电话加密，并存入DTO
        String phoneEntity = entity.getPhone();
        String phoneDTO = phoneEntity.substring(0, 3) + "****" + phoneEntity.substring(8);
        userDTO.setPhone(phoneDTO);

        // 获取用户的生日，如果是空的，则给一个默认值
        String birthday = entity.getBirthday();
        if (StringUtils.isEmpty(birthday)) {
            userDTO.setBirthday("2000-01-01");
        }
        return userDTO;
    }

    /**
     * 如果修改条数是1，则代表修改成功，返回true
     *
     * @return 是否成功
     */
    @Override
    public boolean updateUserInfoById(TUserDTO userDTO) {
        TUserEntity userEntity = ConvertUtils.sourceToTarget(userDTO, TUserEntity.class);
        Integer res = userDao.updateUser(userEntity);
        return res == 1;
    }

    /**
     * 查询用户所有信息，如果用户不存在或已经删除，则抛异常
     *
     * @param username 用户名
     * @return 用户信息
     */
    @Override
    public TUserEntity getUserInfoByUsername(String username) {
        // 用户信息
        TUserEntity user = userDao.selectByUsername(username);
        // 验证用户
        if (user == null || "1".equals(user.getIsDeleted())) {
            throw new BusinessException("用户不存在");
        }
        return user;
    }

    @Override
    public boolean senCodeById(Integer id, String phone) {

        String randomCode = ValidPhone.randomCode().toString();
        JSONObject jsonObject = sendCodeServer.sendCode(phone, randomCode);
        String code = jsonObject.getString("code");
        if (!"0".equals(code)) {
            // todo 重试机制没做
            log.error(code);
        } else {
            redisTemplate.opsForValue().set(String.format(REDIS_CODE_KEY, id, phone), randomCode, Duration.ofSeconds(60 * 5));
        }
        return true;
    }

    @Override
    public boolean replacePhoneById(Integer id, String phone, String code) {
        String userCode = redisTemplate.opsForValue().get(String.format(REDIS_CODE_KEY, id, phone));
        if (StringUtils.isEmpty(userCode)) {
            throw new BusinessException("请获取验证码");
        }
        if (!userCode.equals(code)) {
            throw new BusinessException("验证码输入错误");
        }
        Integer res = userDao.updatePhoneById(id, phone);
        return res == 1;
    }


}