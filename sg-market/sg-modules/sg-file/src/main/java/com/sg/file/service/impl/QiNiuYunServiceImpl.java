package com.sg.file.service.impl;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.sg.file.dto.QiNiuYunDto;
import com.sg.file.service.QiNiuYunService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @author tangjie
 * @create 2023-12-07 13:01
 */
@Service
@Slf4j
public class QiNiuYunServiceImpl implements QiNiuYunService {
    @Autowired
    private UploadManager uploadManager;

    @Autowired
    private QiNiuYunDto qiNiuYunDto;

    public String onLoadFile(InputStream inputStream,String filename,String contentType) {
        String filePath =null;
        //更新之后的文件名，避免文件名冲突
        StringBuilder key = new StringBuilder(filename);
        key.insert(key.lastIndexOf("."),System.currentTimeMillis());

        //默认不指定key的情况下，以文件内容的hash值作为文件名
        Auth auth = Auth.create(qiNiuYunDto.getAk(), qiNiuYunDto.getSk());

        String upToken = auth.uploadToken(qiNiuYunDto.getBucket());
        try {
            Response response = uploadManager.put(inputStream,key.toString(), upToken,null,contentType);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
//            System.out.println(putRet.key);
//            System.out.println(putRet.hash);
            //获取七牛云的访问地址
            filePath = qiNiuYunDto.getBaseUrl() + putRet.key;
        } catch (QiniuException ex) {
            ex.printStackTrace();
            log.debug("文件上传失败！",ex);
        }
        return filePath;
    }
}
