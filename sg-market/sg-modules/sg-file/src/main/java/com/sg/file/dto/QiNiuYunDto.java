package com.sg.file.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author tangjie
 * @create 2023-12-07 16:57
 */
@Component
@ConfigurationProperties("qi-niu")
@Data
public class QiNiuYunDto {
    private String ak;
    private String sk;
    private String bucket;
    private String baseUrl;
}
