package com.sg.file.service;

import java.io.InputStream;

/**
 * @author tangjie
 * @create 2023-12-07 13:01
 */
public interface QiNiuYunService {
    String onLoadFile(InputStream inputStream, String filename, String contentType);
}
