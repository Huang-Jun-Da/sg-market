package com.sg.file.controller;

import com.sg.common.core.utils.Result;
import com.sg.file.service.QiNiuYunService;
import com.sg.file.service.impl.QiNiuYunServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author tangjie
 * @create 2023-12-07 13:00
 */
@RestController
@RequestMapping("/file")
@Slf4j
public class FileOnLoadController{
    @Autowired
    private QiNiuYunService qiNiuYunService;

    @PostMapping("/onLoadFile")
    public Result onLoadFile(MultipartFile file){
        String filePath =null;
        try {
            //获取文件流
            InputStream inputStream = file.getInputStream();
            //获取文件名
            String filename = file.getOriginalFilename();
            //获取文件媒体类型
            String contentType = file.getContentType();
//            System.out.println(inputStream);
//            System.out.println(filename);
//            System.out.println(contentType);
            //调用业务层上传文件
            filePath = qiNiuYunService.onLoadFile(inputStream, filename, contentType);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result().error();
        }
        return new Result().ok(filePath);
    }
}
