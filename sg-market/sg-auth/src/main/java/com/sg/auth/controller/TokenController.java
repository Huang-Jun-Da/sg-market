package com.sg.auth.controller;


import com.sg.auth.form.LoginBody;
import com.sg.auth.form.RegisterBody;
import com.sg.auth.service.SysLoginService;
import com.sg.common.my.utils.Result;
import com.sg.commons.security.annotation.NoLogin;
import com.sg.commons.security.entity.LoginUser;
import com.sg.commons.security.utils.JWTHutoolUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * token 控制
 * 
 * @author ruoyi
 */
@RestController
public class TokenController
{

    @Autowired
    JWTHutoolUtils tokenService;


    @Autowired
    private SysLoginService sysLoginService;


    /**
     * 用户登录
     * @param form
     * @return
     */
    @NoLogin
    @PostMapping("login")
    public Result login(@RequestBody LoginBody form) {
        String token = sysLoginService.login(form.getUsername(), form.getPassword());
        return new Result().ok(token);
    }

//    @DeleteMapping("logout")
//    public R<?> logout(HttpServletRequest request)
//    {
//        String token = SecurityUtils.getToken(request);
//        if (StringUtils.isNotEmpty(token))
//        {
//            String username = JwtUtils.getUserName(token);
//            // 删除用户缓存记录
//            AuthUtil.logoutByToken(token);
//            // 记录用户退出日志
//            sysLoginService.logout(username);
//        }
//        return R.ok();
//    }
//
//    @PostMapping("refresh")
//    public R<?> refresh(HttpServletRequest request)
//    {
//        LoginUser loginUser = tokenService.getLoginUser(request);
//        if (StringUtils.isNotNull(loginUser))
//        {
//            // 刷新令牌有效期
//            tokenService.refreshToken(loginUser);
//            return R.ok();
//        }
//        return R.ok();
//    }
//
//    @PostMapping("register")
//    public R<?> register(@RequestBody RegisterBody registerBody)
//    {
//        // 用户注册
//        sysLoginService.register(registerBody.getUsername(), registerBody.getPassword());
//        return R.ok();
//    }
}
