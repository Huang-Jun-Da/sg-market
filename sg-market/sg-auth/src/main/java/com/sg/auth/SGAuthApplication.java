package com.sg.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableFeignClients(basePackages = "com.sg.feign.api.service.userInfo")
public class SGAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(SGAuthApplication.class, args);
    }

}
