package com.sg.auth.service;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.sg.common.my.exception.BusinessException;
import com.sg.common.my.utils.UUIDUtils;
import com.sg.common.redis.constant.RedisConstants;
import com.sg.common.redis.service.RedisService;
import com.sg.commons.security.utils.JWTHutoolUtils;
import com.sg.feign.api.service.userInfo.RemoteUserInfoService;
import com.sg.feign.api.service.userInfo.TUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 登录校验方法
 * 
 * @author ruoyi
 */
@Component
public class SysLoginService
{
//    @Autowired
//    private RemoteUserService remoteUserService;

//    @Autowired
//    private SysPasswordService passwordService;
//
//    @Autowired
//    private SysRecordLogService recordLogService;

    @Autowired
    JWTHutoolUtils  jwtHutoolUtils;





    @Autowired
    RemoteUserInfoService remoteUserInfoService;

    @Autowired
    private RedisService redisService;

    /**
     * 登录
     */
    public String login(String username, String password)
    {
        // 用户名或密码为空 错误
        if (StrUtil.isAllBlank(username, password))
        {
            throw new BusinessException("用户/密码必须填写");
        }

        TUserEntity loginBody = remoteUserInfoService.usernameGetInfo(username);

        if(ObjectUtil.isEmpty(loginBody)){
            throw new BusinessException("用户不存在");
        }

        if(!loginBody.getPassword().equals(password)){
            throw new BusinessException("用户名或密码有错");
        }

//        RedisConstants.LOGIN_USER

        String uuid = UUIDUtils.getUUID();
        Map map = new HashMap<>();
        map.put("uuid",uuid);
        map.put("userId",loginBody.getId());
        map.put("username",loginBody.getUsername());

        redisService.setCacheMapValue(RedisConstants.LOGIN_USER, uuid, loginBody);

        return jwtHutoolUtils.creat(map);
    }



//
//    public void logout(String loginName)
//    {
//        recordLogService.recordLogininfor(loginName, Constants.LOGOUT, "退出成功");
//    }
//
//    /**
//     * 注册
//     */
//    public void register(String username, String password)
//    {
//        // 用户名或密码为空 错误
//        if (StrUtil.isAllBlank(username, password))
//        {
//            throw new BusinessException("用户/密码必须填写");
//        }
//        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
//                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
//        {
//            throw new BusinessException("账户长度必须在2到20个字符之间");
//        }
//        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
//                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
//        {
//            throw new BusinessException("密码长度必须在5到20个字符之间");
//        }
//
//        // 注册用户信息
//        SysUser sysUser = new SysUser();
//        sysUser.setUserName(username);
//        sysUser.setNickName(username);
//        sysUser.setPassword(SecurityUtils.encryptPassword(password));
//        R<?> registerResult = remoteUserService.registerUserInfo(sysUser, SecurityConstants.INNER);
//
//        if (R.FAIL == registerResult.getCode())
//        {
//            throw new BusinessException(registerResult.getMsg());
//        }
//        recordLogService.recordLogininfor(username, Constants.REGISTER, "注册成功");
//    }
}
