package com.sg.feign.api.factory.userInfo;

import com.sg.feign.api.service.userInfo.RemoteUserInfoService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @ClassName HuangJunDa
 * @Description TODO
 * @date 2023/12/7 20:01
 * @Version 1.0
 */

@Component
public class RemoteUserInfoFallbactory implements FallbackFactory<RemoteUserInfoService> {

    @Override
    public RemoteUserInfoService create(Throwable throwable) {
        return null;
    }
}
