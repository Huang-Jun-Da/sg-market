package com.sg.feign.api.factory;


import com.sg.feign.api.service.RemoteFileService;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务降级处理
 * 
 * @author ruoyi
 */
@Component
public class RemoteFileFallbackFactory implements FallbackFactory<RemoteFileService>
{
    @Override
    public RemoteFileService create(Throwable throwable) {
        return null;
    }
}
