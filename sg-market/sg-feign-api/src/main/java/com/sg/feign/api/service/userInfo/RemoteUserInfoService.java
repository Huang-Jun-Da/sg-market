package com.sg.feign.api.service.userInfo;

import com.sg.common.core.constant.ServiceNameConstants;
import com.sg.common.core.dto.SysAddressDTO;
import com.sg.common.core.utils.Result;
import com.sg.feign.api.factory.RemoteAdminFallbackFactory;
import com.sg.feign.api.factory.userInfo.RemoteUserInfoFallbactory;
import com.sg.feign.api.service.userInfo.TUserEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author tangjie
 * @create 2023-12-05 21:05
 */
@FeignClient(
        value = "sg-user-info",
        fallbackFactory = RemoteUserInfoFallbactory.class,
        path = "/api/user-info/user"
        )
public interface RemoteUserInfoService {
        @GetMapping("/usernameGetInfo/{username}")
        public TUserEntity usernameGetInfo(@PathVariable String username);
}
