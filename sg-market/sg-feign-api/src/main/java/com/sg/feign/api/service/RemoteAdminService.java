package com.sg.feign.api.service;

import com.sg.common.core.constant.ServiceNameConstants;
import com.sg.common.core.dto.SysAddressDTO;
import com.sg.common.core.utils.Result;
import com.sg.feign.api.factory.RemoteAdminFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author tangjie
 * @create 2023-12-05 21:05
 */
@FeignClient(
        contextId = "RemoteAdminService",
        value = ServiceNameConstants.ADMIN_SERVICE,
        fallbackFactory = RemoteAdminFallbackFactory.class,
        path = "/api/sg-admin"
        )
public interface RemoteAdminService {
        @GetMapping("/sys/address/list")
        Result<List<SysAddressDTO>> list();
}
