package com.sg.feign.api.service.userInfo;

import lombok.Data;

import java.util.Date;

/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */
@Data
public class TUserEntity {
    private Integer id;
    /**
     * 学校ID
     */
    private String schoolId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别
     */
    private String sex;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 手机
     */
    private String phone;
    /**
     * 签名
     */
    private String userSign;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除
     */
    private String isDeleted;
}