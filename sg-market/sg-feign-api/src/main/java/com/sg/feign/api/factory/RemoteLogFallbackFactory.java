package com.sg.feign.api.factory;


import com.sg.feign.api.service.RemoteLogService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 日志服务降级处理
 * 
 * @author ruoyi
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteLogService>
{

    @Override
    public RemoteLogService create(Throwable throwable) {
        return null;
    }
}
