package com.sg.feign.api.factory;

import com.sg.common.core.constant.ServiceNameConstants;
import com.sg.common.core.dto.SysAddressDTO;
import com.sg.common.core.utils.Result;
import com.sg.feign.api.service.RemoteAdminService;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author tangjie
 * @create 2023-12-05 21:05
 */
@Component
public class RemoteAdminFallbackFactory implements FallbackFactory<RemoteAdminService> {

        @Override
        public RemoteAdminService create(Throwable throwable) {
                return null;
        }
}
