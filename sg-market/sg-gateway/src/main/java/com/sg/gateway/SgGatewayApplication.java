package com.sg.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SgGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SgGatewayApplication.class, args);
    }

}
