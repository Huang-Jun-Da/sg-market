//package com.sg.gateway.filter;
//
//import com.alibaba.fastjson2.JSON;
//import com.sg.common.my.utils.Result;
//import com.sg.gateway.application.InterceptorPathBean;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.core.io.buffer.DataBuffer;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.http.server.reactive.ServerHttpResponse;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.nio.charset.StandardCharsets;
//import java.util.List;
//
//
///**
// * Gateway:全局过滤器
// * 判断是否已经登录
// */
//@Slf4j
//@Component
//public class MyGlobalFilter implements GlobalFilter, Ordered {
//
//    // 需要拦截的路径
//    @Autowired
//    private InterceptorPathBean interceptorPathBean;
//
//
//    @Autowired
//    RedisTemplate<String, String> redisTemplate;
//
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        //获取请求对象
//        ServerHttpRequest request = exchange.getRequest();
//        List<String> specificPaths = interceptorPathBean.getSpecific();
//
//        List<String> prefixesPaths = interceptorPathBean.getPrefixes();
//        //获得path
//        String path = request.getURI().getPath();
//        String prePath = "/api/";
//        String addPrePath = path.split("/")[2];
//        if(StringUtils.isNotBlank(addPrePath)){
//             prePath = prePath + addPrePath;
//        }
//
//        //是否是直接放行路径
//        if(prefixesPaths.contains(prePath) || specificPaths.contains(path)){
//            return chain.filter(exchange);
//        }
//
//        //获取第一个名为token的请求头
//        String token = request.getHeaders().getFirst("token");
//        // 请求头的token不为null
//        if (StringUtils.isNotBlank(token)) {
//            String redisToken = (String) redisTemplate.opsForHash().get("login", "token");
//            //redis的token不为null&& redis的token值与请求头的token值是否相等
//            if (StringUtils.isNotBlank(redisToken) && redisToken.equals(token)) {
//                //已经登录
//                return chain.filter(exchange);
//            }
//        }
//
//        return noPower(exchange);
//
//    }
//
//    @Override
//    public int getOrder() {
//        //值越小，越优先执行
//        return 1;
//    }
//
//
//
//
//    /**
//     * 网关拒绝，返回Result
//     *
//     * @param
//     */
//    private Mono<Void> noPower(ServerWebExchange serverWebExchange) {
//        // 权限不够拦截
//        serverWebExchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
//        Result data = new Result().error(401,"请先登录");
//        DataBuffer buffer = serverWebExchange.getResponse().bufferFactory().wrap(JSON.toJSONString(data).getBytes(StandardCharsets.UTF_8));
//        ServerHttpResponse response = serverWebExchange.getResponse();
//        response.setStatusCode(HttpStatus.UNAUTHORIZED);
//        //指定编码，否则在浏览器中会中文乱码
//        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
//        return response.writeWith(Mono.just(buffer));
//
//    }
//}
//
//
