package com.sg.gateway.application;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 读取配置文件
 */
@Data
@Component
@ConfigurationProperties(prefix = "releasepath") // 配置文件的前缀
public class InterceptorPathBean {

    //白名单详细路径集合
    private List<String> specific;

    //白名单前缀路径集合
    private List<String> prefixes;
}