package com.sg.commons.security.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName HuangJunDa
 * @Description TODO
 * @date 2023/12/5 19:58
 * @Version 1.0
 */

@Data
public class LoginUser {
    private Integer id;
    private String schoolId;
    private String username;
    private String password;
    private String nickname;
    private String sex;
    private String phone;
    private String userSign;
    private String avatar;
    private Date createTime;
    private Date updateTime;
    private String isDeleted;
}
