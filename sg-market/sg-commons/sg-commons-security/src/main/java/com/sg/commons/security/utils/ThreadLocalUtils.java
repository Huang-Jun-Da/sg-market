package com.sg.commons.security.utils;

import java.util.HashMap;
import java.util.Map;

public class ThreadLocalUtils {




    private static final ThreadLocal<Map<String, Object>> THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 存储
     * @param key
     * @param value
     * @return void [返回类型说明]
     */
    public static void put(String key, Object value) {
        Map<String, Object> map = THREAD_LOCAL.get();
        if (map == null) {
            map = new HashMap<>();
        }
        map.put(key, value);
        THREAD_LOCAL.set(map);
    }

    /**
     * 取值
     * @param key
     * @return T [返回类型说明]
     * @returnt
     */

    public static <T> T get(String key) {
        Map<String, Object> map = THREAD_LOCAL.get();

        if (map != null) {
            return (T) map.get(key);

        }

        return null;
    }

    /**
     * 线程执行完，要及时释放此对象
     */
    public static void release() {
        THREAD_LOCAL.remove();
    }
}
