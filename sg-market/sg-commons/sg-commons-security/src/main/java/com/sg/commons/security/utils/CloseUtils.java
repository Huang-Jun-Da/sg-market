package com.sg.commons.security.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.io.IOException;

/**
 * 关闭资源
 */
@Slf4j
public class CloseUtils {

    /**
     * 所有IO流类的接口，里面提供了统一close方法
     * @param cs 可变长参数
     */
    public static void closeAll(Closeable... cs) {
        //数组的增强for遍历
        for(Closeable c:cs) {
            if(c!=null) {
                try {
                    c.close();
                } catch (IOException e) {
                    log.error("关闭资源失败：{}", ExceptionUtils.getStackTraceInfo(e));
                }
            }
        }
    }
}
