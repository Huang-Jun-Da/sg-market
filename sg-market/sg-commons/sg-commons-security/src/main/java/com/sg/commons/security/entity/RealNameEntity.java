package com.sg.commons.security.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-03
 */
@Data
//@TableName("t_real_name")
public class RealNameEntity {

//    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private String name;
    private String idcard;
    private String isRel;
    private String sex;
    private String birthday;
    private String address;
    private Date createTime;
    private Date updateTime;
}