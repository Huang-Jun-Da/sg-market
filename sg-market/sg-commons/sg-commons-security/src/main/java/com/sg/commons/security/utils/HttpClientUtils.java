package com.sg.commons.security.utils;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * 远程调用url的工具类
 */
public class HttpClientUtils {


    /**
     * 发起get请求
     * @param url 请求路径
     * @param headerMap 请求头部信息
     * @param urlMap 参数map
     * @return
     */
    public static String get(String url, Map<String, String> headerMap, Map<String, String> urlMap) {

        if(StringUtils.isEmpty(url)){
            return null;
        }

        //构建客户端对象
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        //设置参数:___?xxx=xxx & xxx=xxx
        if (MapUtil.isNotEmpty(urlMap)) {
            //添加参数
            String paramData = addParamByGet(urlMap);
            //拼接参数
            url = url + "?" + paramData;
        }

        HttpGet httpGet = new HttpGet(url);

        //设置头部信息信
        setHeaders(httpGet, headerMap);
        //响应体
        CloseableHttpResponse response = null;
        String result = null;
        try {
            //设置响应时间等
            setRequest(httpGet);
            //调用接口，获取响应
            response = httpClient.execute(httpGet);
            //将响应转化为一个结果集
            HttpEntity entity = response.getEntity();
            //将内容转化为字符串，并且设定指定字符集
            result = EntityUtils.toString(entity, "UTF-8");
            //最终将结果集返回
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            CloseUtils.closeAll(response, httpClient);
        }
        return result;
    }

    /**
     * 发起post请求
     * @param url
     * @param headerMap
     * @param map
     * @return
     * @throws IOException
     */
    public static String post(String url, Map<String, String> headerMap, Map<String,String> map) throws IOException {

        if(StringUtils.isEmpty(url)){
            return null;
        }


        //构建客户端对象
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        CloseableHttpResponse response = null;
        String result = null;
        try {
            //创建http post请求对象 这个url可以手动封装一个
            HttpPost post = new HttpPost(url);
            //设置请求头
            setHeaders(post, headerMap);

            //构建参数
            if (MapUtil.isNotEmpty(map)){
                JSONObject jo = new JSONObject();
                //增加参数
                addParam(map,jo);
                post.setEntity(new StringEntity(jo.toString(), ContentType.APPLICATION_JSON));
            }else {
                post.setEntity(new StringEntity(null, ContentType.APPLICATION_JSON));
            }

            //设置响应时间等
            setRequest(post);
            //创建响应对象
            response = httpClient.execute(post);
            /**
             * 由于响应接口返回的是一个字符串,因此需要转换
             * 响应体数据封装在HttpEntity对象中
             */
            result = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭资源
            CloseUtils.closeAll(response, httpClient);
        }
        return result;
    }


    /**
     * get添加参数
     */
    public static String addParamByGet(Map<String,String> urlMap){
        StringBuilder param = new StringBuilder();
        //遍历参数
        for (Map.Entry<String, String> m :urlMap.entrySet()) {
            param.append(m.getKey()).append("=").append(m.getValue()).append("&");
        }
        return String.valueOf(param);
    }

    /**
     * post添加参数
     */
    public static JSONObject addParam(Map < String, String > map, JSONObject jsonObject) {
        for (Map.Entry < String, String > entry: map.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }
        return jsonObject;
    }


    /**
     * 设置头部信息
     */
    public static void setHeaders(HttpRequestBase http, Map<String, String> headerMap){
        //设置文本类型
        Header header = new BasicHeader("Content-Type", "application/json");
        // 构造一个头部请求信息
        http.setHeader(header);
        headerMap.forEach(http::setHeader);
    }

    /**
     * Request设置
     */
    public static void setRequest(HttpRequestBase http){
        //设置响应时间等
        RequestConfig requestConfig = RequestConfig.custom()
                //服务器响应超时时间
                .setSocketTimeout(2000)
                //连接服务器超时时间
                .setConnectionRequestTimeout(2000)
                .build();

        //将配置信息添加到 httpGet 中
        http.setConfig(requestConfig);
    }

}
