package com.sg.commons.security.exception;


import com.sg.commons.security.model.ResponseCode;

public class BusinessException extends RuntimeException {

    private  int code;
    private  String description = "";

    //系统默认处理异常的返回数据
    public BusinessException(){
        super(ResponseCode.Business_Exception.getMessage());
        this.code = ResponseCode.Business_Exception.getCode();
    }

    public BusinessException(String message) {
        super(message);
    }


    public BusinessException(int code, String message, String description) {
        super(message);
        this.code = code;
        this.description = description;
    }

    public BusinessException(ResponseCode errorCode) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
    }

    public BusinessException(ResponseCode errorCode, String description) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = description;
    }

    public int getCode() {
        return code;
    }


    public String getDescription() {
        return description;
    }


}
