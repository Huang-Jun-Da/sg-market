package com.sg.commons.security.interceptor;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.sg.common.redis.utils.RedisUtils;
import com.sg.commons.security.annotation.NoLogin;
import com.sg.commons.security.entity.TUserEntity;
import com.sg.commons.security.exception.BusinessException;
import com.sg.commons.security.utils.JWTHutoolUtils;
import com.sg.commons.security.utils.ThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName HuangJunDa
 * @Description TODO
 * @date 2023/12/5 20:01
 * @Version 1.0
 */
@Slf4j
@Component
public class AuthRequestInterceptor extends HandlerInterceptorAdapter {


    public AuthRequestInterceptor() {
        System.out.println("-----------------AuthRequestInterceptor------------------");
    }

    @Autowired
    JWTHutoolUtils jwtHutoolUtils;
    @Autowired
    private RedisUtils redisUtils;


    // 在请求进入处理器之前回调这个方法
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        //获取方法处理器
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        NoLogin noLogin =
                handlerMethod.getMethod()//这一步是获取到我们要访问的方法
                        //然后根据我们制定的自定义注解的Class对象来获取到对应的注解
                        .getAnnotation(NoLogin.class);

        log.info("===========requestURI:{}",noLogin);
        //如果要访问的方法上加这个注解，那么就说明这个方法不需要拦截，否则就需要进行拦截
        if (!ObjectUtils.isEmpty(noLogin)) {
            return true;
        }

        // 获取请求头
        String token = request.getHeader("token");
        // 请求头不为空进行解析
        if (StrUtil.isBlank(token)) {
            throw new BusinessException("请携带令牌访问");
        }
            // 按照我们和前端约定的格式进行处理
//            if (header.startsWith("Bearer ")) {
//                // 得到令牌
//                String token = header.substring(7);
            // 验证令牌
            try {
                // 令牌的解析这里一定的try起来,因为它解析错误的令牌时,会报错
                // 当然你也可以在自定义的jwtUtil中把异常 try起来,这里就不用写了
                if (!jwtHutoolUtils.verify(token)) {
                    throw new BusinessException("令牌已失效");
                }

                // 根据uuid从redis获取用户信息
                JWT jwt = JWTUtil.parseToken(token);
                String tokenStr = (String) jwt.getPayload("UUID");


                TUserEntity loginUser = (TUserEntity) redisUtils.get(tokenStr);

                if (ObjectUtil.isEmpty(loginUser)) {
                    throw new BusinessException("令牌已失效");
                }

                String userID = (String) jwt.getPayload("userID");
                String userName = (String) jwt.getPayload("userName");

                // 将用户信息存在ThreadLocal中
                ThreadLocalUtils.put("id", userID);
                ThreadLocalUtils.put("userName", userName);
                ThreadLocalUtils.put("LoginUser", loginUser);

            } catch (Exception e) {
                throw new BusinessException("令牌不存在");
            }
//            }

        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        清除ThreadLocal
        ThreadLocalUtils.release();
    }
}
