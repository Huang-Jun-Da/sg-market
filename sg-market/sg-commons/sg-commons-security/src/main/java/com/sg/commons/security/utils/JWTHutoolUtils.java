package com.sg.commons.security.utils;

import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.sg.common.redis.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @ClassName HuangJunDa
 * @Description TODO
 * @date 2023/12/6 17:49
 * @Version 1.0
 */

@Component
public class JWTHutoolUtils {

    @Value("${jwt:signing-key}")
    private String SIGNING_KEY;




    /**
     * 创建token
     * @param map
     * @return
     */
    public  String creat(Map<String, Object> map) {
        map.put("expire_time", System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 15);
        return JWTUtil.createToken(map, SIGNING_KEY.getBytes());
    }

    /**
     * 验证token
     * @param token
     * @return
     */
    public  boolean verify(String token) {
        return JWTUtil.verify(token, SIGNING_KEY.getBytes());
    }


}