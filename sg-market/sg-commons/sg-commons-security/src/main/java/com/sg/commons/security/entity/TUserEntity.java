package com.sg.commons.security.entity;


import lombok.Data;

import java.util.Date;

/**
 * @author wlb wlb@qq.com
 * @since 1.0.0 2023-12-02
 */
@Data
//@TableName("t_user")
public class TUserEntity {
//    @TableId(type = IdType.AUTO)
    private Integer id;
    private String schoolId;
    private String username;
    private String password;
    private String nickname;
    private String sex;
    private String phone;
    private String userSign;
    private String avatar;
    private Date createTime;
    private Date updateTime;
    private String isDeleted;
}