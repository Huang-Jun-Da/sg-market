package com.sg.exception.utils;

import com.sg.exception.utils.CloseUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
public class ExceptionUtils {

    /**
     * 获取e.printStackTrace()的具体信息，赋值给String变量，并返回
     * @param  e Exception
     * @return  e.printStackTrace()中的信息
     */
    public static String getStackTraceInfo(Exception e) {

        StringWriter sw = null;
        PrintWriter pw = null;

        try {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            //将出错的栈信息输出到printWriter中
            e.printStackTrace(pw);
            pw.flush();
            sw.flush();

            return sw.toString();
        } catch (Exception ex) {
            log.error("异常信息提取发生异常：{}",e.getMessage());
            return "异常信息提取发生异常：{}" + e.getMessage();
        } finally {
            CloseUtils.closeAll(sw, pw);
        }


    }
}
