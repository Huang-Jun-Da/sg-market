package com.sg.exception.handler;


import com.sg.exception.exception.BusinessException;
import com.sg.exception.model.ResponseCode;
import com.sg.exception.utils.ExceptionUtils;
import com.sg.exception.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 *
 * @author ruoyi
 */

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler
{
    /**
     * 系统异常捕获
     * @param e 捕获的异常
     * @return 封装的返回对象
     **/
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result systemExceptionHandler(Exception e){
        log.error("系统出现异常：{}", ExceptionUtils.getStackTraceInfo(e));
        return new  Result().error(ResponseCode.ERROR.getCode(),"系统维护中，请稍后再试...");
    }

    /**
     * 业务异常捕获
     * @param e 捕获的异常
     * @return 封装的返回对象
     **/
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public Result businessExceptionHandler(BusinessException e) {
        log.error("业务出现异常：{}",ExceptionUtils.getStackTraceInfo(e));
        return new Result().error(ResponseCode.Business_Exception.getCode(), "" + e.getMessage());
    }
}
