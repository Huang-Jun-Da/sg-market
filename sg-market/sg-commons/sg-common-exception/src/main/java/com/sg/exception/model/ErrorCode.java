package com.sg.exception.model;

/**
 * 错误编码接口，所有错误码都必须实现此接口
 * <p>
 * 错误编码由5位数字组成，前2位为模块编码，后3位为业务编码
 * 如：10001（10代表系统模块，001代表业务代码）
 *
 * @author 13637
 * @since 1.0.0
 */
public interface ErrorCode {

    /**
     * 内部服务器错误
     */
    int INTERNAL_SERVER_ERROR = 500;

    /**
     * 未授权
     */
    int UNAUTHORIZED = 401;

    /**
     * 参数不能为空
     */
    int NOT_NULL = 10001;

    /**
     * 数据库记录已存在
     */
    int DB_RECORD_EXISTS = 10002;

    /**
     * 获取参数失败
     */
    int PARAMS_GET_ERROR = 10003;

    /**
     * 账户密码错误
     */
    int ACCOUNT_PASSWORD_ERROR = 10004;

    /**
     * 账户被禁用
     */
    int ACCOUNT_DISABLE = 10005;

    /**
     * 标识符不能为空
     */
    int IDENTIFIER_NOT_NULL = 10006;

    /**
     * 验证码错误
     */
    int CAPTCHA_ERROR = 10007;

    /**
     * 子菜单已存在
     */
    int SUB_MENU_EXIST = 10008;

    /**
     * 密码错误
     */
    int PASSWORD_ERROR = 10009;

    /**
     * 上级部门错误
     */
    int SUPERIOR_DEPT_ERROR = 10011;

    /**
     * 上级菜单错误
     */
    int SUPERIOR_MENU_ERROR = 10012;

    /**
     * 数据范围参数错误
     */
    int DATA_SCOPE_PARAMS_ERROR = 10013;

    /**
     * 部门下存在子部门
     */
    int DEPT_SUB_DELETE_ERROR = 10014;

    /**
     * 部门下存在用户
     */
    int DEPT_USER_DELETE_ERROR = 10015;

    /**
     * 上传文件为空
     */
    int UPLOAD_FILE_EMPTY = 10019;

    /**
     * Token不能为空
     */
    int TOKEN_NOT_EMPTY = 10020;

    /**
     * Token无效
     */
    int TOKEN_INVALID = 10021;

    /**
     * 账户被锁定
     */
    int ACCOUNT_LOCK = 10022;

    /**
     * OSS上传文件失败
     */
    int OSS_UPLOAD_FILE_ERROR = 10024;

    /**
     * Redis操作失败
     */
    int REDIS_ERROR = 10027;

    /**
     * 任务调度错误
     */
    int JOB_ERROR = 10028;

    /**
     * 包含无效字符
     */
    int INVALID_SYMBOL = 10029;
}