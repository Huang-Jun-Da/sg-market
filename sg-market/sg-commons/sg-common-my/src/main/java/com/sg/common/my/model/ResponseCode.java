package com.sg.common.my.model;

public enum ResponseCode {

    /**
     * 1xx：信息，请求收到，继续处理
     * 2xx：成功，行为被成功地接受、理解和采纳
     * 3xx：重定向，为了完成请求，必须进一步执行的动作
     * 4xx：客户端错误，请求包含语法错误或者请求无法实现
     * 5xx：服务器错误，服务器不能实现一种明显无效的请求
     */

    /**参数相关的正确码：2开头**/
    SUCCESS(200,"操作成功!"),


    /**参数相关的错误码：1开头**/
    PARAM_ERROR(1000,"参数异常"),


    /**客户端相关的错误码：4开头**/
    PARAMS_ERROR(4000,"请求参数错误"),
    NULL_ERROR(4001,"请求参数为空"),
    NO_LOGIN(40100,"未登录"),
    NO_AUTH(40101,"暂无权限访问"),


    /**系统相关的错误码：5开头**/

    FAILURE(500,"操作失败"),
    ERROR(5000,"系统异常，请稍后重试"),
    Business_Exception(5001,"服务器异常"),
    INVALID_TOKEN(5001,"访问令牌不合法"),
    ACCESS_DENIED(5002,"没有权限访问该资源"),
    USERNAME_OR_PASSWORD_ERROR(5003,"用户名或密码错误");



    private final int code;

    private final String message;

    ResponseCode(int code, String message){
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}

