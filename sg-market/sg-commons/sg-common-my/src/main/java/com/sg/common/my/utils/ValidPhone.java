package com.sg.common.my.utils;

import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 13637
 */
public class ValidPhone {
    /**
     * 验证手机号码是否是1[3-9]开头的11位手机号
     * 如果不是返回false
     * @param phoneNumber 手机号
     * @return 是否正确
     */
    public static boolean isChinesePhone(String phoneNumber) {
        String regex = "1[3-9]\\d{9}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    /**
     * 获取6位数的随机验证码
     * @return 验证码
     */
    public static Integer randomCode() {
        int min = 100000;
        int max = 999999;
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
