package com.sg.common.core.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author 13637
 */
public class StringToMD5Utils {
    /**
     * 字符串转MD5加密工具类
     *
     * @param original 原始字符串
     * @return 加密后的字符串
     */
    public static String toMD5(String original) {
        try {
            // 创建MD5加密器
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 对字符串进行加密，返回字节数组
            byte[] digest = md.digest(original.getBytes());
            // 将字节数组转换为十六进制的字符串
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b));
            }
            // 返回加密后的字符串
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5加密失败", e);
        }
    }
}
