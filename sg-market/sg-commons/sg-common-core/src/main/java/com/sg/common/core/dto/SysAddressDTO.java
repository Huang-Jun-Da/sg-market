package com.sg.common.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
@Data
@ApiModel(value = "记录地址信息")
public class SysAddressDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "地址id")
	private Integer id;

	@ApiModelProperty(value = "省份")
	private String province;

	@ApiModelProperty(value = "城市")
	private String city;

	@ApiModelProperty(value = "学校")
	private String school;

	@ApiModelProperty(value = "地址添加时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date addTime;


}