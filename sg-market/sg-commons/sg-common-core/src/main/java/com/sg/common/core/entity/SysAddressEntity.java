package com.sg.common.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 记录地址信息
 *
 * @author tangjie tangjie@qq.com
 * @since 1.0.0 2023-12-05
 */
@Data
@TableName("sys_address")
public class SysAddressEntity {

    /**
     * 地址id
     */
    @TableId(type = IdType.AUTO)
	private Integer id;
    /**
     * 省份
     */
	private String province;
    /**
     * 城市
     */
	private String city;
    /**
     * 学校
     */
	private String school;
    /**
     * 地址添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date addTime;
}